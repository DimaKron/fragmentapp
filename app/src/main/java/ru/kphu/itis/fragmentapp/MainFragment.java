package ru.kphu.itis.fragmentapp;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class MainFragment extends Fragment implements View.OnClickListener {

    private static final String KEY = "key";

    private static final String TAG_NEXT_STEP = "NextStep";

    private Button button;

    public static MainFragment newInstance(String data) {
        Bundle args = new Bundle();
        args.putString(KEY, data);
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        button = (Button) view.findViewById(R.id.button);
        button.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        NextStepFragment fragment = new NextStepFragment();
        fragment.show(getFragmentManager(), TAG_NEXT_STEP);
    }
}
