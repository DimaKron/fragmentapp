package ru.kphu.itis.fragmentapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends FragmentHostActivity {

    @Override
    protected Fragment getFragment() {
        return MainFragment.newInstance("data");
    }
}
