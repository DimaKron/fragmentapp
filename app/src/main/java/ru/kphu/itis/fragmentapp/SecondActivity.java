package ru.kphu.itis.fragmentapp;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SecondActivity extends FragmentHostActivity {

    public static Intent makeIntent(Context context){
        return new Intent(context, SecondActivity.class);
    }

    @Override
    protected Fragment getFragment() {
        return SecondFragment.newInstance();
    }
}
